# README #

### What is this repository for? ###

* Quick summary: Manhattan plot with ggplot from GAPIT output
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up : drop your GAPIT output (.csv) in the 3G folder. Read the manual
* Configuration : noting special
* Dependencies : python 3+ , R 3.2.3 (package ggplot2 and RColorBrewer)
* Database configuration : main GAPIT output (.csv) [something like: GAPIT..rprac.GWAS.Results.csv]
* Deployment instructions : use chmod u+x *.sh prior to do anything else

### Contribution guidelines ###

* Code: Rémi Maglione

### Who do I talk to? ###

* Repo owner: remi.maglione@gmail.com