#!/usr/bin/env python
__author__ = 'Remi Maglione'

import argparse
import sys

# python version control
try:
    assert sys.version_info[0] == 3
except AssertionError:
    print("== Error ==  python version 3 only supported!")
    sys.exit(1)

# Arguments
parser = argparse.ArgumentParser(description=
                                 "Trim a fastq file in 3' with the correct adapter based on a list of adapter")
parser.add_argument('-i', '--input-csv', type=str, required=True,
                    help='Name of the input .csv file.\nMust contain: 1- the chromosome number\n'
                         '\n If no input is proved, it\'ll take all csv file from the current dir')

parser.add_argument('-o', '--output-csv', type=str, required=False,
                    help='Name of the output .csv file'
                         '\nIf no input is provided, it\'ll add a «csv_» prefix to all output')

# Basic control of the argument
try:
    args = parser.parse_args()
except:
    sys.exit()

# Fonction


def csvopen(infile):
    return open(infile, mode='r')


if __name__ == '__main__':
    with csvopen(args.input_csv) as f:
        with open(args.output_csv, "w") as out:
            lastChrPos = 0
            chrPos2Add = 0
            oldChr = 1

            while True:
                # read the file
                try:
                    chr, pos, pval = f.readline().split(',')

                    try:
                        type(int(chr)) # use int value only
                    except:
                        continue

                    # Control the chr
                    if oldChr == int(chr):
                        posInt = int(pos) + chrPos2Add  # add the position of previous chr
                        chaine = "{},{},{}".format(chr, posInt, pval)
                        out.writelines(chaine)      # Write Chaine to out
                        lastChrPos = posInt         # Memorize last position
                        oldChr = int(chr)           # add current chr to oldchr

                    elif oldChr != int(chr):
                        posInt = int(pos) + lastChrPos  # add the position of previous chr
                        chaine = "{},{},{}".format(chr, posInt, pval)
                        out.writelines(chaine)
                        oldChr = int(chr)           # add current chr to oldchr

                        chrPos2Add = lastChrPos     # last position become position to add
                        lastChrPos = posInt         # Memorize the last position

                except:
                    print("[1] MAP ---> OK")
                    break