###__author__ = 'Remi Maglione' ###

#####
##3G.sh
#####

library("ggplot2")
library("RColorBrewer")
## fonction pour la palette de couleur: special thanks to Simon O'Hanlon
mypal <- colorRampPalette( brewer.pal( 9 , "Set1" ))

## Argument/Data
args <- commandArgs(trailingOnly = TRUE)
GWAS <- read.csv(file=args[1], header = TRUE, sep=',')
#GWAS <- read.csv(file='/home/haclionex/Documents/GAPIT-Remi/GAPITmap.csv', header = FALSE, sep=',')
colnames(GWAS) <- c('chr', 'pos', 'pval')

## Trouver la position centrale du label de chaque chromosome sur l'axe X
chrnumber=20
chrNameMid <- vector(length=chrnumber)
for (i in 1:chrnumber){tmp <- which(GWAS[, 1]==i)
chrPos <- GWAS[tmp, 2]
chrNameMid[i] <- ((max(chrPos) - min(chrPos))/2) + min(chrPos)
}

## manhattan plot
tiff("GWASmanhattan.tiff", width=3720, height=880, res=480)
print(
ggplot(data=GWAS, aes(x=pos, y=-log10(pval), colour=as.factor(chr), alpha=-log10(pval))) +
geom_bar(stat='identity', position='identity') + theme(legend.position='none') +
scale_x_continuous(labels=as.character(1:chrnumber), breaks=chrNameMid)  +
scale_colour_manual( values = mypal(20) ) + 
ylim(c(0, 15)) + xlab('') + theme(panel.background = element_blank()) +
##geom_hline(yintercept = 4, linetype=8, col='red', lwd=1.5) +
theme(text=element_text(size=14))
)
garbage <- dev.off()
print("manhattan ---> OK")
