#!/bin/bash

# variable
file=$1

# Main
Rscript order.R $file GWASo.csv
python madGAPIT1.py -i GWASo.csv -o GAPITmap.csv
Rscript allUniqPlot.R GWASo.csv
Rscript GWAS.R GAPITmap.csv	

##__author__ = 'Remi Maglione' ###
